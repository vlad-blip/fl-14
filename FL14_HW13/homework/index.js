//=== TASK 1 START ===
function getAge(birthday){
    let [birthYear, birthMonth, birthDate] = birthday.toLocaleDateString().split('-');
    let [currentYear, currentMonth, currentDate] = new Date().toLocaleDateString().split('-');
    currentMonth = Number(currentMonth);
    birthMonth = Number(birthMonth);
    if (currentMonth > birthMonth || currentMonth === birthMonth && currentDate >= birthDate){
        return currentYear - birthYear;
    }
    return currentYear - birthYear - 1;
}
//=== TASK 1 END ===

//=== TASK 2 START ===
function getWeekDay(date){
    const dateConverted = new Date(date);
    const options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' };
    const dateSplited = dateConverted.toLocaleDateString('en', options).split(',');
    return dateSplited[0];
}
//=== TASK 2 END ===

//=== TASK 3 START ===
function getProgrammersYear(year){
    const date = new Date(year, 0, 256);
    const options = { weekday: 'long', year: 'numeric', month: 'short', day: 'numeric' };
    const dateSplited = date.toLocaleDateString('en', options).split(',');
    const [, month, day] = dateSplited[1].split(' ');
    const weekday = getWeekDay(date);
    return `${day} ${month}, ${year} (${weekday})`;
}
//=== TASK 3 END ===

//=== TASK 4 START ===
function howFarIs(specifiedWeekday){
    const date = new Date()
    const weekdayArr = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];
    const weekdayNumNext = weekdayArr.indexOf(specifiedWeekday.toLowerCase());
    const weekdayNumCurr = date.getDay();
    let daysLeft = weekdayNumNext - weekdayNumCurr;
    if (daysLeft === 0){
        return `Hey, today is ${specifiedWeekday} =)`;
    } else if (daysLeft < 0) {
        daysLeft = 6 - weekdayNumCurr + weekdayNumNext + 1;
    }
    return `It's ${daysLeft} day(s) left till ${specifiedWeekday}.`;
}
//=== TASK 4 END ===

//=== TASK 5 START ===
function isValidIdentifier(variable){
    const regex = /^[a-zA-Z_$]+[\d_$]*$/;
    return regex.test(variable);
}
//=== TASK 5 END ===

//=== TASK 6 START ===
function capitalize(str){
    const regex = /\b[a-z]/gi;
    return str.replace(regex, letter => letter.toUpperCase());
}

//=== TASK 6 END ===

//=== TASK 7 START ===
function isValidAudioFile(fileName){
    const regex = /^[a-zA-Z]+\.(mp3|flac|alac|aac)$/;
    return regex.test(fileName);
}
//=== TASK 7 END ===

//=== TASK 8 START ===
function getHexadecimalColors(str){
    const regex = /(#[a-zA-Z0-9]{3}|#[a-zA-Z0-9]{6})(?=;)/g;
    return str.match(regex) || [];
}
//=== TASK 8 END ===

//=== TASK 9 START ===
function isValidPassword(password){
    const regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/;
    return regex.test(password);
}
//=== TASK 9 END ===

//=== TASK 10 START ===
function addThousandsSeparators(num){
    return String(num).replace(/(\d)(?=(\d{3})+$)/g, '$1,');
}
//=== TASK 10 END ===