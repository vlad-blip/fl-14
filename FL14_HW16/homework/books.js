const books = [
  {
    name: 'The Handmaid\'s Tale ',
    author: 'Margaret Atwood',
    image: 'https://images-na.ssl-images-amazon.com/images/I/41p0u2G9hbL._SX322_BO1,204,203,200_.jpg',
    plot: 'An instant classic and eerily prescient cultural phenomenon, ' + 
    'from “the patron saint of feminist dystopian fiction” (New York Times). ' + 
    'Now an award-winning Hulu series starring Elizabeth Moss. '
  },
  {
    name: 'Brave New World',
    author: 'Aldous Huxley',
    image: 'https://images-na.ssl-images-amazon.com/images/I/41le8ej-fiL._SX327_BO1,204,203,200_.jpg',
    plot: 'Now more than ever: Aldous Huxley\'s enduring masterwork must be read' + 
    ' and understood by anyone concerned with preserving the human spirit'
  },
  {
    name: 'Animal Farm',
    author: 'George Orwell',
    image: 'https://images-na.ssl-images-amazon.com/images/I/41NzDuSdIfL._SX277_BO1,204,203,200_.jpg',
    plot: 'George Orwell\'s timeless and timely allegorical novel—a scathing satire on a downtrodden ' + 
    'society’s blind march towards totalitarianism.'
  }
];
localStorage.setItem('defaultBooksList', JSON.stringify(books));