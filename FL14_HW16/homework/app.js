const root = document.getElementById('root');
const defaultBooksList = JSON.parse(localStorage.getItem('defaultBooksList'));
const baseUrl = window.location.href;
const leftSection = document.createElement('section');
const rightSection = document.createElement('section');
const addBookBtn = document.createElement('button');
let booksListUl = document.createElement('ul');

function Book(name, author, image, plot){
  this.name = name;
  this.author = author;
  this.image = image;
  this.plot = plot;
}

Book.prototype.addBook = function(){
  const books = JSON.parse(localStorage.getItem('booksList'));
  books.push(this);
  localStorage.setItem('booksList', JSON.stringify(books));
}

function displayBook(){
  if (localStorage.getItem('booksList') === null) {
    localStorage.setItem('booksList', JSON.stringify(defaultBooksList));
  }
  const books = JSON.parse(localStorage.getItem('booksList'));
  books.forEach((book, index) => {
    if (document.getElementById(`book-${index}`) === null){
      const li = document.createElement('li');
      li.id = `book-${index}`;
      li.setAttribute('data-id', index)
      li.innerHTML = `<p>Name: ${book.name}</p><p>Author: ${book.author}<p><img src='${book.image}'>
      <p>Plot: ${book.plot}</p><button id='edit-button'>Edit</button>`;
      booksListUl.appendChild(li);
    }
  });
}

function previewBook(uniqueId){
  const booksList = JSON.parse(localStorage.getItem('booksList'));
  const exists = rightSection.getElementsByTagName('div')[0];
  if (exists !== undefined){
    exists.remove();
  }
  const div = document.createElement('div');
  const name = document.createElement('h1');
  const author = document.createElement('p');
  const image = document.createElement('img');
  const plot = document.createElement('p');
  name.innerText = booksList[uniqueId].name;
  div.id = 'previewDiv';
  author.innerText = booksList[uniqueId].author;
  image.setAttribute('src', booksList[uniqueId].image);
  plot.innerText = booksList[uniqueId].plot;
  div.appendChild(name);
  div.appendChild(author);
  div.appendChild(image);
  div.appendChild(plot);
  rightSection.appendChild(div);
}

function showForm(author='', name='', image='', plot=''){
  const exists = rightSection.getElementsByTagName('div')[0];
  if (exists !== undefined){
    exists.remove();
  }
  const div = document.createElement('div');
  const inputName = document.createElement('input');
  const inputAuthor = document.createElement('input');
  const inputImage = document.createElement('input');
  const inputPlot = document.createElement('input');
  const labelName = document.createElement('label');
  const labelAuthor = document.createElement('label');
  const labelImage = document.createElement('label');
  const labelPlot = document.createElement('label');
  const saveBtn = document.createElement('button');
  const cancelBtn = document.createElement('button');
  labelAuthor.innerText = 'Author: ';
  labelName.innerText = 'Name: ';
  labelImage.innerText = 'Image: ';
  labelPlot.innerText = 'Plot: ';
  inputAuthor.value = author;
  inputName.value = name;
  inputImage.value = image;
  inputPlot.value = plot;
  inputName.id = 'name';
  inputAuthor.id = 'author';
  inputImage.id = 'image';
  inputPlot.id = 'plot';
  labelName.htmlFor = 'name';
  labelAuthor.htmlFor = 'author';
  labelImage.htmlFor = 'image';
  labelPlot.htmlFor = 'plot';
  saveBtn.innerText = 'Save';
  cancelBtn.innerText = 'Cancel';
  div.id = 'addDiv';
  div.appendChild(labelName);
  div.appendChild(inputName);
  div.appendChild(labelAuthor);
  div.appendChild(inputAuthor);
  div.appendChild(labelImage);
  div.appendChild(inputImage);
  div.appendChild(labelPlot);
  div.appendChild(inputPlot);
  div.appendChild(saveBtn);
  div.appendChild(cancelBtn);
  rightSection.appendChild(div);

  return {
    saveBtn: saveBtn,
    cancelBtn: cancelBtn,
    inputName: inputName,
    inputAuthor: inputAuthor,
    inputImage: inputImage,
    inputPlot: inputPlot
  }
}

function addNewBook(){
  const formObj = showForm();
  formObj.saveBtn.addEventListener('click', () => {
    const book = new Book(formObj.inputName.value, formObj.inputAuthor.value, 
      formObj.inputImage.value, formObj.inputPlot.value);
    book.addBook();
    displayBook();
    rightSection.getElementsByTagName('div')[0].remove();
  })
}


addBookBtn.textContent = 'Add new book';
addBookBtn.classList.add('add-book');
booksListUl.classList.add('book-list');
leftSection.classList.add('left-section');
rightSection.classList.add('right-section');
leftSection.appendChild(booksListUl);
leftSection.appendChild(addBookBtn);
root.appendChild(leftSection);
root.appendChild(rightSection);

displayBook();

booksListUl.addEventListener('click', event => {
  const bookHtml = event.target.parentElement;
  const uniqueId = Number(bookHtml.dataset['id']);
  const booksList = JSON.parse(localStorage.getItem('booksList'));
  if (event.target.id === 'edit-button'){
    const book = booksList[uniqueId];
    const formObj = showForm(book.author, book.name, book.image, book.plot);

    history.pushState({book: uniqueId}, `editing book ${uniqueId}`, `?id=${uniqueId}#edit`);
    window.dispatchEvent(new Event('popstate'));

    formObj.saveBtn.addEventListener('click', () => {
      booksList[uniqueId] = {
        name: formObj.inputName.value,
        author: formObj.inputAuthor.value,
        image: formObj.inputImage.value,
        plot: formObj.inputPlot.value
      }
      localStorage.setItem('booksList', JSON.stringify(booksList));
      previewBook(uniqueId);
      const editEbook = booksList[uniqueId];
      bookHtml.innerHTML = `<p>Name: ${editEbook.name}</p><p>Author: ${editEbook.author}<p>
      <img src='${editEbook.image}'>
      <p>Plot: ${book.plot}</p><button id='edit-button'>Edit</button>`;
      setTimeout(() => alert('Book succesfully updated!'), 200);
    });

    formObj.cancelBtn.addEventListener('click', () => {
      const result = window.confirm('Discard changes?');
      console.log(result);
      if (result){
        history.back();
        rightSection.getElementsByTagName('div')[0].remove();
      }
    });

  } else {
    history.pushState({book: uniqueId}, `book ${uniqueId}`, `?id=${uniqueId}#preview`);
    window.dispatchEvent(new Event('popstate'));
  }
});

addBookBtn.addEventListener('click', () => {
  history.pushState('adding new book', `adding new book`, `#add`);
  window.dispatchEvent(new Event('popstate'));
  addNewBook();
});

window.addEventListener('popstate', () => {
  if (window.location.hash === '#preview'){
    const regex = /\?id=(\d+)#preview$/;
    const uniqueId = window.location.href.match(regex)[1];
    previewBook(uniqueId);
  }
});