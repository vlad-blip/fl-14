'use strict';

function isNumberValid(num) {
    return num > 0 && !isNaN(num);
}

function isPercentageValid(num) {
    return isNumberValid(num) && num <= 100;
}

for (;;) {
    const numBatteries = parseInt(prompt('Enter amount of batteries: '));

    if (isNumberValid(numBatteries) === false) {
        alert('Invalid input data');
        break;
    }

    const percentage = parseInt(prompt('Enter percentage of defective batteries: '));

    if (isPercentageValid(percentage) === false) {
        alert('Invalid input data');
        break;
    }

    const numDefective = numBatteries * percentage / 100;
    const numWorking = numBatteries - numDefective;
    alert('Amount of butteries: ' + numBatteries +
    '\nDefective rate: ' + percentage +
    '\nAmount of defective batteries: ' + numDefective.toFixed(2) +
    '\nAmount of working batteries: ' + numWorking.toFixed(2));
    
}
