//=== TASK 1 START ===
const isEquals = (a, b) => a === b;
//=== TASK 1 END ===

//=== TASK 2 START ===
const numberToString = a => String(a);
//=== TASK 2 END ===

//=== TASK 3 START ===
function storeNames (){
  return [...arguments];
}
//=== TASK 3 END ===

//=== TASK 4 START ===
const getDivision = (a, b) => b > a ? b/a : a/b;
//=== TASK 4 END ===

//=== TASK 5 START ===
const negativeCount = arr => arr.filter(num => num < 0).length;
//=== TASK 5 END ===

//=== TASK 6 START ===
function letterCount(word, letter){
  let count = 0;
  const arr = word.split('');
  arr.forEach(el => {
    if(el === letter){
      count++;
    }
  });
  return count;
}
//=== TASK 6 END ===

//=== TASK 7 START ===
function countPoints(arr){
  let points = 0;
  arr.forEach((game) => {
    let gameSplitted = game.split(':');
    if (Number(gameSplitted[0]) > Number(gameSplitted[1])){
      points += 3;
    } else if (Number(gameSplitted[0]) === Number(gameSplitted[1])){
      points++;
    }
  });
  return points;
}
//=== TASK 7 END ===