const data = [
  {
    'folder': true,
    'title': 'Grow',
    'children': [
      {
        'title': 'logo.png'
      },
      {
        'folder': true,
        'title': 'English',
        'children': [
          {
            'title': 'Present_Perfect.txt'
          }
        ]
      }
    ]
  },
  {
    'folder': true,
    'title': 'Soft',
    'children': [
      {
        'folder': true,
        'title': 'NVIDIA',
        'children': null
      },
      {
        'title': 'nvm-setup.exe'
      },
      {
        'title': 'node.exe'
      }
    ]
  },
  {
    'folder': true,
    'title': 'Doc',
    'children': [
      {
        'title': 'project_info.txt'
      }
    ]
  },
  {
    'title': 'credentials.txt'
  }
];

const rootNode = document.getElementById('root');
const mainFolderTree = document.createElement('ul');
const folderWindow = document.createElement('div');
folderWindow.classList.add('folderView');

function contextMenu(){
    const menuDiv = document.createElement('div');
    const removeDiv = document.createElement('button');
    const renameDiv = document.createElement('button');
    menuDiv.id = 'context-menu';
    removeDiv.classList.add('item', 'remove');
    renameDiv.classList.add('item', 'rename');
    removeDiv.innerText = 'Remove';
    renameDiv.innerText = 'Rename';
    menuDiv.appendChild(removeDiv);
    menuDiv.appendChild(renameDiv);
    document.body.appendChild(menuDiv);
}

function idGenerator(){
    let id = '';
    for (let i = 0; i < 5; i++){
        id += Math.floor(Math.random() * 100);
    }
    return id;
}

function displayTree(array, listParent){
  if (array === null){
      let listElement = document.createElement('li');
      listElement.innerHTML = `<em>Folder is Empty</em>`;
      listParent.appendChild(listElement);
  } else {
      for (let element of array){
          let icon = '';
          let htmlClass = ''
          let listElement = document.createElement('li');
          if (element.folder === true){
              icon = 'folder';
              htmlClass = icon;
          } else {
              icon = 'insert_drive_file';
              htmlClass = 'file';
          }
          let dataId = idGenerator();
          listElement.innerHTML = `<p class='${htmlClass} tree_element' data-id='${dataId}'>
          <i class='material-icons ${icon}_icon'>${icon}</i>${element.title}</p>`;
          listParent.appendChild(listElement);
          if (element.hasOwnProperty('children') === true){
              let list = document.createElement('ul');
              listParent.appendChild(list);
              list.classList.add('closed');
              displayTree(element.children, list);
          }
      }
  }
}

function removeText(){
    let element = document.querySelector(`[data-id='${elementId}']`);
    element.remove();
}

contextMenu();
let elementId = '';
window.addEventListener('contextmenu',function(event){
    event.preventDefault();
    let itemsList = Array.from(document.getElementsByClassName('item'));
    itemsList.forEach(item => item.removeAttribute('disabled'));
    let contextElement = document.getElementById('context-menu');
    contextElement.style.top = event.pageY + 'px';
    contextElement.style.left = event.pageX + 'px';
    if (event.target.classList.contains('tree_element')){
        elementId = event.target.getAttribute('data-id');
    } else {
        let itemsList = Array.from(document.getElementsByClassName('item'));
        itemsList.forEach(item => item.setAttribute('disabled' ,''));
    }
    contextElement.classList.add('active');
});
window.addEventListener('click',function(){
    document.getElementById('context-menu').classList.remove('active');
});

document.getElementById('context-menu').addEventListener('click', event => {
    if (event.target.classList.contains('remove')){
        removeText();
    }
});

displayTree(data, mainFolderTree);
folderWindow.appendChild(mainFolderTree)
rootNode.appendChild(folderWindow);
const treeElements = Array.from(document.querySelectorAll('.tree_element'));
treeElements.forEach(el => {
    el.addEventListener('click', () => {
        if (el.classList.contains('folder')){
            let folderElements = el.parentElement.nextElementSibling
            folderElements.classList.toggle('closed');
            if (folderElements.classList.contains('closed')){
                el.firstChild.innerHTML = 'folder';
            } else {
                el.firstChild.innerHTML = 'folder_open';
            }
        }
    });
});