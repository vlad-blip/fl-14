//========== TASK 1 START ==========
function convert(arr){
    const convertedArr = [];
    for (let i = 0; i < arr.length; i++){
        let element = arr[i];
        if (typeof element === 'string'){
            element = parseInt(element);
        } else {
            element = element.toString();
        }
        convertedArr.push(element);
    }
    return convertedArr;
}
//========== TASK 1 END ==========

//========== TASK 2 START ==========
function executeforEach(arr, callbackfunction){
    for (let i = 0; i < arr.length; i++){
        callbackfunction(arr[i]);
    }
}
//========== TASK 2 END ==========

//========== TASK 3 START ==========
function mapArray(arr, callbackfunction=0){
    const convertedArr = [];
    executeforEach(arr, (el) => { 
        convertedArr.push(callbackfunction(parseInt(el))) 
    });
    return convertedArr;
}
//========== TASK 3 END ==========

//========== TASK 4 START ==========
function filterArray(arr, callbackfunction){
    const convertedArr = [];
    executeforEach(arr, (el) => { 
        if (callbackfunction(parseInt(el)) === true){
            convertedArr.push(el);
        }
    });
    return convertedArr;
}
//========== TASK 4 END ==========

//========== TASK 5 START ==========
function getValuePosition(arr, value){
    for (let i = 0; i < arr.length; i++){
        if (arr[i] === value){
            return i+1;
        }
    }
    return false;
}
//========== TASK 5 END ==========

//========== TASK 6 START ==========
function flipOver(string){
    let newStr = '';
    for (let i = 0; i < string.length; i++){
        newStr += string[string.length - 1 - i];
    }
    return newStr;
}
//========== TASK 6 END ==========

//========== TASK 7 START ==========
function makeListFromRange(arr){
    let [start, end] = arr;
    const newArr = [];
    if (start > end){
        [start, end] = [end, start];
        for (let i = start; i <= end; i++){
            newArr.unshift(i);
        }
    } else{
        for (let i = start; i <= end; i++){
            newArr.push(i);
        }
    }
    return newArr;
}
//========== TASK 7 END ==========

//========== TASK 8 START ==========
const fruits = [
  {name: 'apple', weight: 0.5},
  {name: 'pineapple', weight: 2}
];
function getArrayOfKeys(arr, key){
  const newArr = [];
  executeforEach(arr, function(el){
    newArr.push(el[key]);
  });
  return newArr;
}
//========== TASK 8 END ==========

//========== TASK 9 START ==========
const basket = [
    { name: 'Bread', weight: 0.3 },
    { name: 'Coca-Cola', weight: 0.5 },
    { name: 'Watermelon', weight: 8 }
  ];

function getTotalWeight(arr){
    let weight = 0;
    executeforEach(arr, function(el){
      weight += el.weight;
    });
    return weight;
}
//========== TASK 9 END ==========

//========== TASK 10 START ==========
const date = new Date(2020, 0, 2);
function getPastDay(date, days){
    const pastDate = new Date(date.getTime());
    pastDate.setDate(date.getDate() - days);
    const month = pastDate.toLocaleString('en', { month: 'short' });
    return `${pastDate.getDate()}, (${pastDate.getDate()} ${month} ${pastDate.getFullYear()})`;
}
//========== TASK 10 END ==========

//========== TASK 11 START ==========
function formatDate(dateObj){
    const date = {
        year : dateObj.getFullYear(),
        month: dateObj.getMonth() + 1,
        day: dateObj.getDate(),
        hours: dateObj.getHours(),
        minutes: dateObj.getMinutes()
    };
    for (let key in date){
        if (date[key] > 0 && date[key] < 10){
            date[key] = '0' + date[key];
        }
    }
    return `${date.year}/${date.month}/${date.day} ${date.hours}:${date.minutes}`;
}
//========== TASK 11 END ==========