const main = document.getElementById('main');
const totalPrize_span = document.getElementById('total_prize');
const currentRoundPrize_span = document.getElementById('current_round_prize');
const skip_btn = document.getElementById('skip');
const newGame_btn = document.getElementById('new_game');
const variant1_btn = document.getElementById('variant-1');
const variant2_btn = document.getElementById('variant-2');
const variant3_btn = document.getElementById('variant-3');
const variant4_btn = document.getElementById('variant-4');
const question_title = document.getElementById('question');
const endMessage_p = document.getElementById('end-message');

let currentPrize = 0;
let roundPrize = 100;
let questionObject;
let usedQuestion;
const questionsArr = JSON.parse(localStorage.getItem('questions'));

function randomQuestion(){
    for (;;) {
        const randomIndex = Math.floor(Math.random() * questionsArr.length);
        if (usedQuestion.includes(randomIndex)) {
            continue;
        } else {
            usedQuestion.push(randomIndex);
            return questionsArr[randomIndex];
        }
    }
}

function showQuestion(){
    question_title.innerHTML = questionObject.question;
    document.getElementById('variant-1').innerHTML = questionObject.content[0];
    document.getElementById('variant-2').innerHTML = questionObject.content[1];
    document.getElementById('variant-3').innerHTML = questionObject.content[2];
    document.getElementById('variant-4').innerHTML = questionObject.content[3];
}

function win(){
    currentPrize += roundPrize;
    roundPrize *= 2;
    totalPrize_span.innerHTML = currentPrize;
    currentRoundPrize_span.innerHTML = roundPrize;
    if (currentPrize >= 1000000) {
        endMessage('win');
    }
}

function endMessage(result) {
    if (result === 'win') {
        endMessage_p.innerHTML = `Congratulations you won 1000000!`;
    } else {
        endMessage_p.innerHTML = `Game over. Your prize is: ${currentPrize}`;
    }
    main.classList.add('hidden');
    endMessage_p.classList.remove('hidden');
    skip_btn.classList.add('hidden');
}

function lose(){
    endMessage('lose');
}

function game(answer){
    const correct = questionObject.correct;
    if (answer === correct) {
        win();
        questionObject = randomQuestion();
        showQuestion();
    } else {
        lose();
    }
}

function gameStart(){
    usedQuestion = [];
    currentPrize = 0;
    roundPrize = 100;
    endMessage_p.classList.add('hidden');
    skip_btn.classList.remove('hidden');
    skip_btn.removeAttribute('disabled', '');
    main.classList.remove('hidden');
    totalPrize_span.innerHTML = currentPrize;
    currentRoundPrize_span.innerHTML = roundPrize;
    questionObject = randomQuestion();
    showQuestion(questionObject);
}

newGame_btn.addEventListener('click', () => {
    gameStart();
});

skip_btn.addEventListener('click', () => {
    questionObject = randomQuestion()
    showQuestion(questionObject);
    skip_btn.setAttribute('disabled', '');
});

variant1_btn.addEventListener('click', () => {
    game(0);
});

variant2_btn.addEventListener('click', () => {
    game(1);
});

variant3_btn.addEventListener('click', () => {
    game(2);
});

variant4_btn.addEventListener('click', () => {
    game(3);
});
