/* START TASK 1: Your code goes here */
const table = document.getElementById('table');
const tableRows = Array.from(table.rows);

function checkIfNotYellow(row){
  for (let i = 0; i < row.length; i++){
    if (row[i].classList.contains('yellow')){
      return false;
    }
  }
  return true;
}

const allCells = [];
for (let i = 0; i < tableRows.length; i++){
  for (let j = 0; j < tableRows[i].cells.length; j++){
    allCells.push(tableRows[i].cells[j]);
  }
}

tableRows.forEach((row) => {
  const cellsArr = Array.from(row.cells);
  cellsArr.forEach((cell, cellIndex) => {
    cell.addEventListener('click', event => {
      if(cell.id === 'specialCell'){
        allCells.forEach(cell => {
          if (cell.classList.length === 0){
            cell.style.backgroundColor = 'green';
            cell.classList.add('green');
          }
        });
      } else if (cellIndex === 0 && checkIfNotYellow(cellsArr)){
        cellsArr.forEach(cell => {
          cell.style.backgroundColor = 'blue';
          cell.classList.add('blue');
        })
      } else {
        event.target.style.backgroundColor = 'yellow';
        event.target.classList.add('yellow');
      }
    });
  });
});
/* END TASK 1 */


/* START TASK 2: Your code goes here */
const inputForm = document.getElementById('number');
const submitBtn = document.getElementById('button');
const notificationText = document.getElementById('notification-text');

function notificationFailure(){
  const parentEl = notificationText.parentElement;
  notificationText.innerText = 'Type number doesn\'t follow format +380*********';
  parentEl.classList.add('failure');
  inputForm.classList.add('failure');
  parentEl.classList.remove('hidden');
}

function notificationSuccess(){
  const parentEl = notificationText.parentElement;
  notificationText.innerText = 'Data sent successfully';
  parentEl.classList.add('success');
  inputForm.classList.add('success');
  parentEl.classList.remove('failure');
  parentEl.classList.remove('hidden');
}
function isValid(number){
  const regex = /^\+380\d{9}$/;
  return regex.test(number);
}

inputForm.addEventListener('input', () => {
  if (isValid(inputForm.value) === true){
    submitBtn.removeAttribute('disabled', '');
    inputForm.classList.remove('failure');
    notificationText.parentElement.classList.toggle('hidden');
  } else {
    submitBtn.setAttribute('disabled', '');
    notificationFailure();
  }
});

submitBtn.addEventListener('click', (event) => {
  event.preventDefault();
  if (isValid(inputForm.value) === true){
    notificationSuccess();
  }
});
/* END TASK 2 */

/* START TASK 3: Your code goes here */
const courtDiv = document.getElementById('court-block');
const ballImg = document.getElementById('ball');
const goalZones = Array.from(document.getElementsByClassName('goal'));
const scoreB = document.getElementById('score-b');
const scoreA = document.getElementById('score-a');
const message = document.getElementById('message');

const scores = {
  'goal-a': 0,
  'goal-b': 0
};

function updateScore(scored){
  scores[scored]++;
  scoreA.innerText = scores['goal-a'];
  scoreB.innerText = scores['goal-b'];
}

function displayMessage(scored){
  if (scored === 'goal-a'){
    message.style.color = 'blue';
    message.innerText = 'Team A score!';
  } else {
    message.style.color = 'red';
    message.innerText = 'Team B score!';
  }
  message.style.display = 'block';
  setTimeout(() => {
    message.style.display = 'none';
  }, 3000);
}

courtDiv.addEventListener('click', event => {
  ballImg.style.top = event.pageY + 'px';
  ballImg.style.left = event.pageX + 'px';
});

goalZones.forEach(goal => {
  goal.addEventListener('click', () => {
    displayMessage(goal.id);
    updateScore(goal.id);
  });
});
/* END TASK 3 */