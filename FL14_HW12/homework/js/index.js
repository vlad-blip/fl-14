function visitLink(path) {
	let visitedNum = localStorage.getItem(path);
	if (visitedNum){
		visitedNum++;
		localStorage.setItem(path, visitedNum);
	} else {
		localStorage.setItem(path, 1);
	}
}

function viewResults() {
	const list = document.createElement('ul');
	for (let i = 0; i < localStorage.length; i++){
		let key = localStorage.key(i);
		let li = document.createElement('li');
		li.appendChild(document.createTextNode(`You visited ${key} ${localStorage.getItem(key)} time(s)`));
		list.appendChild(li);
	}
	document.getElementById('content').appendChild(list);
	localStorage.clear();
}
