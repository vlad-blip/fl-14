while(true){
  const expression = prompt('Enter expression');
  try {
    const result = eval(expression);
    if (typeof result === 'undefined'){
        throw new Error('You didn\'t enter anything!');
    } else if (isNaN(result) || !isFinite(result)){
        throw new Error('Zero division error!');
    } else if (result === null){
        break;
    }
    alert(result);
    break;
  } catch(e){
    if (e instanceof ReferenceError){
        alert('Enter only numbers!');
    } else if (e instanceof SyntaxError){
        alert('Enter full expression!');
    } else {
        alert(e.message);
    }
  }
}
